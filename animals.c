///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   28 January 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

   // @todo Map the enum Color to a string

   char *color_str[] = {
      "Black",
      "White",
      "Red",
      "Blue",
      "Green",
      "Pink"
   }; 
   
   return color_str[color];

   return NULL; // We should never get here
};

char* genderStr (enum Gender gender) {

   char *gender_str[] = {
      "Male",
      "Female"
   };

   return gender_str[gender];
}
